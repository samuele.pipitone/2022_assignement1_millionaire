CREATE DATABASE assignement1;

USE assignement1;

CREATE TABLE questions (
    questionId int NOT NULL AUTO_INCREMENT,
    question varchar(255) NOT NULL,
    difficultyLevel int,
    PRIMARY KEY (QuestionId)
);

CREATE TABLE answers (
    answersId int NOT NULL AUTO_INCREMENT,
    answer varchar(255) NOT NULL,
    question varchar(255) NOT NULL,
    isCorrect boolean,
    PRIMARY KEY (answersId),
    FOREIGN KEY (answersId) REFERENCES questions(questionId) ON DELETE CASCADE
);

SET FOREIGN_KEY_CHECKS = 0;