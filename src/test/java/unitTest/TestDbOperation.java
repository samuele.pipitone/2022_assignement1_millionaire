package test.java.unitTest;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import main.java.model.Answer;
import main.java.model.Question;
import main.java.model.Stage;

public class TestDbOperation {

	@org.junit.Test
	public void stageTest() {
		
		Stage stage = createStage();
		List<Answer> answers = createAnswers();
		
		assertEquals(stage.getQuestion().getQuestion(), createQuestion()
		        .getQuestion());
		assertEquals(stage.getAnswers().size(), answers.size());
		assertEquals(stage.getAnswers().get(0).getAnswer(), answers.get(0)
		        .getAnswer());
	}
	
	public Stage createStage() {
		Stage stage = new Stage();
		stage.setAnswers(createAnswers());
		stage.setQuestion(createQuestion());
		return stage;
	}
	
	public List<Answer> createAnswers() {
		List<Answer> answers = new ArrayList<>();
		
		Answer answer = new Answer();
		Answer answer2 = new Answer();
		Answer answer3 = new Answer();
		
		answer.setAnswer("prova");
		answer.setAnswerId(1);
		answer.setCorrect(false);
		answer2.setAnswer("test");
		answer2.setAnswerId(2);
		answer2.setCorrect(false);
		answer3.setAnswer("mock");
		answer3.setAnswerId(3);
		answer3.setCorrect(true);
		
		answers.add(answer);
		answers.add(answer2);
		answers.add(answer3);
		return answers;
	}
	
	public Question createQuestion() {
		Question question = new Question();
		
		question.setDifficultyLevel(1);
		question.setIndex(1);
		question.setQuestion("prova");
		
		return question;
	}

}
