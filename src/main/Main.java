package main;

import java.util.List;
import java.util.Scanner;

import main.java.DB.MyDbOperations;
import main.java.model.Question;
import main.java.model.Stage;

public class Main {

	public static void main(String[] args) {
		
		String replay = "yes";
		String role = "";
		String insertQuestion = "";
		boolean isFirstTime = true;
		int adminCase = 0;
		int insertDifficultyLevel = 0;
		int index = 0;
		int deleteQuestion;
		int level = 1;
		int response;
		boolean win = false;
		boolean isRight = true;
		
		Scanner scanner = new Scanner(System.in);
		Scanner scannerInt = new Scanner(System.in);
		MyDbOperations myDb = new MyDbOperations();
		List<Question> questions;
		Stage currentStage;
		
		while(!replay.equalsIgnoreCase("no") && !replay.equalsIgnoreCase("n")) {
		    
		    isRight = true;
		    win = false;
			
			if(isFirstTime) {
				System.out.println("Welcome to the Millionaire game! \n");
				System.out.println("Are you an admin or a player? \n");
				
				role = scanner.nextLine();
				isFirstTime = false;
				
				while(!role.equalsIgnoreCase("admin") && !role.equalsIgnoreCase("player")) {
					System.out.println("You can only select between admin and player! \n");
					System.out.println("Please enter a valid input");
					role = scanner.nextLine();
				}
			}
			
			//ADMIN
			if(role.equalsIgnoreCase("admin")) {
				System.out.println("Would you like to: \n");
				System.out.println("0) Exit");
				System.out.println("1) Insert a question");
				System.out.println("2) Delete a question");
				
				adminCase = scannerInt.nextInt();
				switch(adminCase) {
					case 0: {
						break;
					}
					case 1: {
						System.out.println("Please insert a question: \n");
						insertQuestion = scanner.nextLine();
						System.out.println("Perfect! Now insert a number of difficulty from 1 to 8: \n");
						insertDifficultyLevel = scannerInt.nextInt();
						
						if(insertDifficultyLevel < 0 || insertDifficultyLevel > 8) {
							System.out.println("Not a valid nuber!");
						} else {
							myDb.insertQuestion(insertQuestion, insertDifficultyLevel);
						}
						break;
					}
					case 2: {
						questions = myDb.showQuestions();
						System.out.println("Which question would you like to delete? ");
						
						for(Question q: questions) {
							index++;
							q.setIndex(index);
							System.out.println(index +") " + q.getQuestion());
						}
						
						deleteQuestion = scannerInt.nextInt();
						
						for(Question q: questions) {
							
							if(q.getIndex() == deleteQuestion) {
								
								if(myDb.deleteQuestion(q.getIndex())) {
									System.out.println("Successfully deleted");
								} 
								else {
									System.out.println("There was a problem");
								}
							} 

						}
						break;
					}
					default: {
						System.out.println("You inserted an invalid number");
					}
				}
			}
			else { //PLAYER
				while(level <= 8 && isRight) {
					System.out.println("You are at level " + level + "\n");
					
					currentStage = myDb.getStage(level);
					
					System.out.println(currentStage.getQuestion().getQuestion() + ":\n");
					System.out.println("1) " + currentStage.getAnswers().get(0).getAnswer());
					System.out.println("2) " + currentStage.getAnswers().get(1).getAnswer());
					System.out.println("3) " + currentStage.getAnswers().get(2).getAnswer());
					System.out.println("4) " + currentStage.getAnswers().get(3).getAnswer());
					
					response = scannerInt.nextInt();
					
					if(currentStage.getAnswers().get(response - 1).isCorrect()) {
					    isRight = true;
						if(level == 8) {
							win = true;
						}
						System.out.println("Correct! \n");
						level++;
					} else {
					    isRight = false;
					}
				}
				
				if(win) {
					System.out.println("Congratulations, you WON!");
				} else {
					System.out.println("Wrong answer! Sorry \n");
				}
			}
			
			System.out.println("Would you like to play again?");
			level = 1;
			
			replay = scanner.nextLine();
		}
		
		scanner.close();
		scannerInt.close();
	}
}

