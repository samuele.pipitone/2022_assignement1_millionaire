package main.java.model;

public class Answer {

	private int answerId;
	private String answer;
	private int question;
	private boolean isCorrect;
	
	public Answer() {}

	public Answer(int answerId, String answer, boolean isCorrect) {
		this.answerId = answerId;
		this.answer = answer;
		this.isCorrect = isCorrect;
	}

	public int getAnswerId() {
		return answerId;
	}

	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public int getQuestion() {
		return question;
	}

	public void setQuestion(int question) {
		this.question = question;
	}

	public boolean isCorrect() {
		return isCorrect;
	}

	public void setCorrect(boolean isCorrect) {
		this.isCorrect = isCorrect;
	}
	
}
