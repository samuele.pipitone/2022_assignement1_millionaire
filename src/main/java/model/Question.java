package main.java.model;

public class Question {
	
	private int questionId;
	private String question;
	private int difficultyLevel;
	private int index;
	
	public Question() {}

	public Question(int questionId, String question, int difficultyLevel) {
		this.questionId = questionId;
		this.question = question;
		this.difficultyLevel = difficultyLevel;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public int getDifficultyLevel() {
		return difficultyLevel;
	}

	public void setDifficultyLevel(int difficultyLevel) {
		this.difficultyLevel = difficultyLevel;
	}

}
