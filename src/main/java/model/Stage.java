package main.java.model;

import java.util.List;

public class Stage {
	
	private Question question;
	private List<Answer> answers;
	
	public Stage() {}

	public Stage(Question question, List<Answer> answers) {
		this.question = question;
		this.answers = answers;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

}
