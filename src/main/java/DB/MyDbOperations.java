package main.java.DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import main.java.model.Answer;
import main.java.model.Question;
import main.java.model.Stage;

public class MyDbOperations {
	
	private final String USER = "root";
	private final String PASSWORD = "root";
	private final String connectionUrl = "jdbc:mysql://192.100.0.000:3306/assignement1";
	
	public MyDbOperations() {}
	
	public Question insertQuestion(String question, int difficulty) {
		Question q = new Question();
		String insert = "INSERT INTO questions (question, difficultyLevel) VALUE(?, ?)";
		
		try {
			//create connection
			Connection conn = DriverManager.getConnection(connectionUrl, USER, PASSWORD);
			
			//create query
			PreparedStatement preparedStatement = conn.prepareStatement(insert);
			preparedStatement.setString(1, question);
			preparedStatement.setInt(2, difficulty);
			
			//execute query
			preparedStatement.execute();
			
			System.out.println("Question inserted! \n");
			
			//close connection
			conn.close();
			
		} catch (SQLException e) {
		    e.printStackTrace();
		}
		
		return q;
	}

	public List<Question> showQuestions() {
		
		List<Question> questions = new ArrayList<Question>();
		String query = "SELECT * FROM questions";
		Question q;
		
		try {
				//create connection
				Connection conn = DriverManager.getConnection(connectionUrl, USER, PASSWORD);
				
				//create query
				PreparedStatement preparedStatement = conn.prepareStatement(query);
				
				//execute query
				ResultSet rs = preparedStatement.executeQuery();
				
				//cycle results and build list
				while(rs.next()) {
					q = new Question(rs.getInt("questionId"), rs.getString("question") , rs.getInt("difficultyLevel"));
					questions.add(q);
				}
				
				//close connection
				conn.close();
				
			} catch (SQLException e) {
			    e.printStackTrace();
			}
		return questions;
	}
	
	public boolean deleteQuestion(int questionId) {
		
		String query = "DELETE FROM questions WHERE questionId = ?";
		String queryAnswer = "DELETE FROM answers WHERE answersId = ?";
		
		try {
			//create connection
			Connection conn = DriverManager.getConnection(connectionUrl, USER, PASSWORD);
			
			//create query
			PreparedStatement preparedStatement = conn.prepareStatement(query);
			preparedStatement.setInt(1, questionId);
			
			PreparedStatement preparedStatementAnswer = conn.prepareStatement(queryAnswer);
			preparedStatement.setInt(1, questionId);
			
			//execute query
			preparedStatement.execute();
			preparedStatementAnswer.execute();
			
			//close connection
			conn.close();
			return true;
			
		} catch (SQLException e) {
		    return false;
		}
	}
	
	public Stage getStage(int level) {
		Stage stage = new Stage();
		Question question = new Question();
		Answer currentAnswer;
		List<Answer> answers = new ArrayList<>();
		
		String query = "SELECT * FROM questions WHERE difficultyLevel = ? ORDER BY RAND () LIMIT 1";
		String queryAnswer = "SELECT * FROM answers WHERE question = ?";
		
		try {
			//create connection
			Connection conn = DriverManager.getConnection(connectionUrl, USER, PASSWORD);
			
			//create query
			PreparedStatement preparedStatement = conn.prepareStatement(query);
			preparedStatement.setInt(1, level);
		
			//execute query
			ResultSet rs = preparedStatement.executeQuery();
			
			if(rs.next()) {
				question.setDifficultyLevel(level);
				question.setQuestion(rs.getString("question"));
				question.setQuestionId(rs.getInt("questionId"));
			}
			
			//create query
			PreparedStatement preparedStatementAnswer = conn.prepareStatement(queryAnswer);
			preparedStatementAnswer.setInt(1, question.getQuestionId());
			
			//execute query
			ResultSet rsAnswer = preparedStatementAnswer.executeQuery();
			
			while(rsAnswer.next()) {
				currentAnswer = new Answer(rsAnswer.getInt("answersId"), rsAnswer.getString("answer"), rsAnswer.getBoolean("isCorrect"));
				answers.add(currentAnswer);
			}
			
			//close connection
			conn.close();
			
		} catch (SQLException e) {
		    e.printStackTrace();
		}
		
		stage.setAnswers(answers);
		stage.setQuestion(question);
		
		return stage;
	}

}