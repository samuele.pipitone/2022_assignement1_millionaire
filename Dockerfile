FROM openjdk:11-jdk-slim
ENV TZ=Europe/Rome

WORKDIR /srv/app/
COPY ./target/Millionaire-0.0.1-SNAPSHOT.jar ./Millionaire-0.0.1-SNAPSHOT.jar

EXPOSE 8080
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "Millionaire-0.0.1-SNAPSHOT.jar"]
