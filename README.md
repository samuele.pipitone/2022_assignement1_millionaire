# Assignement 1

## Membri:
	- Marco Campione 844555
	- Samuele Pipitone 830595
## Repository:
	- https://gitlab.com/samuele.pipitone/2022_assignement1_millionaire
## Applicazione:
Millionair simula il gioco "Chi vuol essere milionario", il gioco consiste nel dover rispondere ad alcune domande chiuse, ciascuna con 4 possibili scelte. Ogni domanda giusta 		alzerà 	il livello di difficoltà della successiva domanda. Per vincere bisogna rispondere correttamente ad 8 domande di fila. E' anche disponibile un pannello admin per 			aggiungere o rimuovere domande. Il focus dell'assignement non era lo sviluppo dell'app quindi molti dettagli sono stati lasciati al caso, abbiamo utilizzato Maven come project 	manager e MySql come Database.
## Build:
Utilizziamo il comando "mvn $MAVEN_OPTS $MAVEN_CLI_OPTS compile" per creare i file .class dell'applicazione.
## Verify
Viene analizzato il codice per verificare non ci siano errori "di stile". Viene utilizzato "allow_failure: true" per non bloccare la pipeline per semplici "errori" di stile.
## Test
Stage che comprende i due job "Unit test e Integration Test".
### Unit test
Viene eseguito in test di unità sulla classe TestDbOperation con JUnit e maven-surefire.
### Integration test
Viene eseguito in test d'integrazione sulla classe ITMySql con JUnit e maven-failsafe.
Abbiamo avuto dei problemi a creare un database in locale e non avevamo a disposizione un server per fare funzionare correttamente questo test perció abbiamo deciso comunque di tenerlo, aggiungendo il comando "allow_failure: true" per proseguire nell'esecuzione della pipeline.
## Package
Utilizziamo il comando "mvn $MAVEN_CLI_OPTS $MAVEN_OPTS clean package" per creare il file .jar dell'applicazione e lo mettiamo nella cartella /target.
## Release
Rilasciamo il .jar del job precedente nel nostro profilo su Docker Hub
## Deploy
Facciamo il deploy dell'immagine di docker su un droplet di DigitalOcean.
Successivamente facciamo l'accesso al server di DigitalOcean ed eseguiamo l'applicazione in detached mode.
